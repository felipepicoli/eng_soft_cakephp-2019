# ENG_SOFT_cakePHP-2019-LUIZ_FELIPE_PICOLI
# CakePHP 

O CakePHP 3 é um framework de desenvolvimento web que funciona com o PHP 7.3 ou no mínimo com o PHP 5.6.0. Leia a seção CakePHP num piscar de olhos para ter uma ideia geral dos fundamentos do CakePHP 3.
O cookbook do CakePHP é um projeto de documentação editável aberto à comunidade.
Observe o botão com um ícone de um lápis no canto direito, ele vai direcioná-lo para o editor online do GitHub referente a página atual, 
permitindo que você facilmente contribua com quaisquer adições, exclusões ou correções para a documentação.

No site oficial tem o tutarial em portugues.

## Primeiros passos

Aprender um novo framework pode ser intimidador e excitante ao mesmo tempo. 
Para ajudar você nesse processo, nós criamos um cookbook recheado de exemplos e receitas que remetem tarefas rotineiras.
Se você é iniciante, deveria começar com o Guia de Início Rápido que vai lhe proporcionar um panorama geral sobre o que o 
CakePHP tem a oferecer e seu funcionamento.
Após concluir o tutorial do Bookmarker, você pode se aprofundar sobre os principais elementos existentes em uma aplicação construída com o CakePHP:

## O ciclo de requisição do CakePHP

As convenções que o CakePHP utiliza.
Controllers lidam com requisições e coordenam seus models com as respostas que sua aplicação gera.
Views são a camada de apresentação da sua aplicação. Elas te oferecem poderosas ferramentas para criar HTML, JSON e as outras saídas que sua aplicação precisa.
Models são o ingrediente principal em qualquer aplicação. Eles lidam com a validação e a lógica de domínio em sua aplicação.

## Vantagens de utilisar o cakePHP

-Compativel com PHP 4 e 5;

-Curva de aprendizado alta;

-Compativel com varios bancos de dadoss(Mysqsl,Postgres,SQlite,SQL Server,Oracle,OBDC,Firebird);

-Componentes nativos para email,sessão,criptografia,autorização,ACl, etc;

-Internacionalização;

-Geração de CRUD para interação com banco de dados;

-Arquitetura MVC;

-Validação;

-Templates;

-Ferramentas que auxiliam gerar JavaScrip, AJAX, Forms HTML, etc

## Desvantagem

-Está evoluindo ainda na utilização de OO (Orientação a Objeto). 

-Pouco opcional para sistemas robustos. 

-Pouco treinamento especifico para o CakePHP.

## Conclusão

Desenvolver usando frameworks facilita a padronização e trabalho em equipe. Acelera o ciclo de desenvolvimento.
Cake é fácil de aprender.
Cake usa uma estrutura de arquivos prática e simples.

## Licença

CakePHP é licenciado sob a licença MIT. Isso significa que você é livre para modificar, distribuir e publicar o código-fonte com a condição de manter os avisos de direitos autorais intactos. Você também é livre para incorporar CakePHP em qualquer aplicativo de origem comercial ou fechada.


[![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app)
[![Total Downloads](https://img.shields.io/packagist/dt/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

A skeleton for creating applications with [CakePHP](https://cakephp.org) 3.x.

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).
## Requisitos

-HTTP Server. Por exemplo: Apache. De preferência com mod_rewrite ativo, mas não é obrigatório.
PHP 5.6.0 ou superior.

-extensão mbstring

-extensão intl
## Instalação

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `php composer.phar create-project --prefer-dist cakephp/app [app_name]`.

Se o Composer estiver instalado globalmente, execute

```bash
composer create-project --prefer-dist cakephp/app
```

Caso você queira usar um nome de diretório de aplicativo personalizado (por exemplo, `/ myapp /`):

```bash
composer create-project --prefer-dist cakephp/app myapp
```

Agora você pode usar o servidor da sua máquina para exibir a página inicial padrão ou iniciar
o servidor web incorporado com:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Update

Desde que este esqueleto é um ponto de partida para o seu aplicativo e vários arquivos
teria sido modificado de acordo com as suas necessidades, não há uma maneira de fornecer
atualizações automáticas, então você precisa fazer atualizações manualmente.

## Configuração

Leia e edite `config / app.php` e configure o` 'Datasources'` e qualquer outro
configuração relevante para sua aplicação.

## Layout

O esqueleto do aplicativo usa um subconjunto de [Foundation] (http://foundation.zurb.com/) (v5) CSS
framework por padrão. Você pode, no entanto, substituí-lo por qualquer outra biblioteca ou
estilos personalizados.

## clone

Para clonar o projoto:git clone git@gitlab.com:felipepicoli/eng_soft_cakephp-2019.git

## Crianção do Crud

-comendo para a instalação do projeto:

-composer create-project --prefer-dist cakephp/app [app_name]

-Feito isto ja foi criado seu projeto.

-Antes de criar o banco de dados tem quer fazer a conecção com o bancos de dados.

-EX:

-Vai em:c:\wamp64\www\exemplo\projeto01\config\app.php e altera este arquivo

-na linha 254 altera o campo

-username 'root'

-password ''

-datebase 'aula'

-Agora vai em localhost para criar o banco de dados.

-Agora execute o comando abaixo para a criação da tabelo no banco de dados.

-bin\cake bake all [nome da tabela]

Após a instalação do cakePHP vai gerar o projeto onde você pode começar criar seu site.Abre o promt de comendo do Windows navega ate o seu projeto.
Agora com CMD do windows entra no projeto e navega ate  pasta bin Feito isto cria um banco de dados no localhost phpmyadmin e depois vai promt comento do windows e colocar este comando
cake base all e mais o nome do banco de dados que voce criou.Feito voce ja criou seu CRUD.

## Video sobre instalação e configuração do CakePHP

https://youtu.be/mg-GxgY5ktk


